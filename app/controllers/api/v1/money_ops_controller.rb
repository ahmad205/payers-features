module Api
  module V1
    class MoneyOpsController < ApplicationController
      before_action :set_money_op, only: [:show, :edit, :update, :destroy]
      skip_before_action :verify_authenticity_token

      # GET /money_ops.json
      def index
        
        @money_ops = MoneyOp.all
        respond_to do |format|
          format.json { render json: @money_ops }
        end

        # =begin
        # @api {get} /api/v1/money_ops 1-Request Money Operations List
        # @apiVersion 0.3.0
        # @apiName GetMoneyOperation
        # @apiGroup Money Operations
        # @apiExample Example usage:
        # curl -i http://localhost:3000/api/v1/money_ops
        # @apiSuccess {Number} id Money Operation unique ID.
        # @apiSuccess {String} opid Operation unique number.
        # @apiSuccess {Number} optype Operation type (1 for Sent & 2 for Receive).
        # @apiSuccess {Number} amount Operation amount.
        # @apiSuccess {Number} payment_gateway  Operation payment gateway (paypal,bitcoin,...).
        # @apiSuccess {Number} status Operation status (0 for pending & 1 for compeleted).
        # @apiSuccess {Date} payment_date  Payment operation creation date.
        # @apiSuccess {Number} payment_id Payment ID for the operation itself.
        # @apiSuccess {Number} user_id Operation user.
        # @apiSuccess {Date} created_at  Date created.
        # @apiSuccess {Date} updated_at  Date Updated.
        # @apiSuccessExample Success-Response:
        # HTTP/1.1 200 OK
        # [
        #   {
        #       "id": 1,
        #       "opid": "PYZPRV18M7NT",
        #       "optype": 1,
        #       "amount": "150",
        #       "payment_gateway": 1,
        #       "status": 0,
        #       "payment_date": "2018-08-01T09:52:00.000Z",
        #       "payment_id": 3,
        #       "user_id": 1,
        #       "created_at": "2018-08-01T09:52:20.462Z",
        #       "updated_at": "2018-08-01T09:52:20.462Z"
        #   },
        #   {
        #       "id": 2,
        #       "opid": "PYTUGN80WB39",
        #       "optype": 1,
        #       "amount": "50",
        #       "payment_gateway": 1,
        #       "status": 0,
        #       "payment_date": "2018-08-01T09:52:00.000Z",
        #       "payment_id": 2,
        #       "user_id": 1,
        #       "created_at": "2018-08-01T09:52:56.776Z",
        #       "updated_at": "2018-08-01T09:52:56.776Z"
        #   }
        # ]
        # @apiError MissingToken invalid Token.
        # @apiErrorExample Error-Response:
        # HTTP/1.1 400 Bad Request
        #   {
        #     "error": "Missing token"
        #   }
        # =end
      end

      # GET /money_ops/1.json
      def show
        # =begin
        # @api {get} /api/money_ops/{:id} 3-Request Specific Money Operation
        # @apiVersion 0.3.0
        # @apiName GetSpecificMoneyOperation
        # @apiGroup Money Operations
        # @apiExample Example usage:
        # curl -i http://localhost:3000/api/v1/money_ops/2
        # @apiParam {Number} id Card Operation ID.
        # @apiSuccess {Number} id Money Operation unique ID.
        # @apiSuccess {String} opid Operation unique number.
        # @apiSuccess {Number} optype Operation type (1 for Sent & 2 for Receive).
        # @apiSuccess {Number} amount Operation amount.
        # @apiSuccess {Number} payment_gateway  Operation payment gateway (paypal,bitcoin,...).
        # @apiSuccess {Number} status Operation status (0 for pending & 1 for compeleted).
        # @apiSuccess {Date} payment_date  Payment operation creation date.
        # @apiSuccess {Number} payment_id Payment ID for the operation itself.
        # @apiSuccess {Number} user_id Operation user.
        # @apiSuccess {Date} created_at  Date created.
        # @apiSuccess {Date} updated_at  Date Updated.
        # @apiSuccessExample Success-Response:
        # HTTP/1.1 200 OK
        #   {
        #       "id": 2,
        #       "opid": "PYTUGN80WB39",
        #       "optype": 1,
        #       "amount": "50",
        #       "payment_gateway": 1,
        #       "status": 0,
        #       "payment_date": "2018-08-01T09:52:00.000Z",
        #       "payment_id": 2,
        #       "user_id": 1,
        #       "created_at": "2018-08-01T09:52:56.776Z",
        #       "updated_at": "2018-08-01T09:52:56.776Z"
        #   }
        # @apiError CardNotFound The id of this Money Operation was not found. 
        # @apiErrorExample Error-Response1:
        # HTTP/1.1 404 Not Found
        #   {
        #     "error": "Card Not Found"
        #   }
        # @apiError MissingToken invalid token.
        # @apiErrorExample Error-Response2:
        # HTTP/1.1 400 Bad Request
        #   {
        #     "error": "Missing token"
        #   }
        # =end

        flash[:error]= "Money Operation Not Found"
        respond_to do |format|
          if @money_op
            format.json { render json: @money_op }
          else
            format.json { render json: flash }
          end
        end

      end

      # GET /money_ops/new
      def new
        @money_op = MoneyOp.new
      end


      # POST /money_ops.json
      def create

        # =begin
        # @api {post} /api/v1/money_ops 2-Create a new Money Operation
        # @apiVersion 0.3.0
        # @apiName PostMoneyOperation
        # @apiGroup Money Operations
        # @apiExample Example usage:
        # curl -X POST \
        # http://localhost:3000/api/v1/money_ops \
        # -H 'cache-control: no-cache' \
        # -H 'content-type: application/json' \
        # -d '{
        # "optype": 1,
        # "amount": "150",
        # "payment_gateway": 1,
        # "status": 0,
        # "payment_date": "2018-08-01T13:31:00.000Z",
        # "payment_id": 3,
        # "user_id": 1
        # }'
        # @apiSuccess {Number} id Money Operation unique ID.
        # @apiSuccess {String} opid Operation unique number (Created automatically and must be 12 digits and letters ).
        # @apiSuccess {Number} optype Operation type (1 for Sent & 2 for Receive).
        # @apiSuccess {Number} amount Operation amount.
        # @apiSuccess {Number} payment_gateway  Operation payment gateway (paypal,bitcoin,...).
        # @apiSuccess {Number} status Operation status (0 for pending & 1 for compeleted).
        # @apiSuccess {Date} payment_date  Payment operation creation date.
        # @apiSuccess {Number} payment_id Payment ID for the operation itself.
        # @apiSuccess {Number} user_id Operation user.
        # @apiSuccess {Date} created_at  Date created.
        # @apiSuccess {Date} updated_at  Date Updated.
        # @apiSuccessExample Success-Response:
        # HTTP/1.1 200 OK
        # {
        #   "id": 8,
        #   "opid": "PYL42W60CTGM",
        #   "optype": 1,
        #   "amount": "150",
        #   "payment_gateway": 1,
        #   "status": 0,
        #   "payment_date": "2018-08-01T13:31:00.000Z",
        #   "payment_id": 3,
        #   "user_id": 1,
        #   "created_at": "2018-08-01T11:33:49.055Z",
        #   "updated_at": "2018-08-01T11:33:49.055Z"
        # }
        # @apiError ExistingOperationId Operation Id has already been taken.
        # @apiErrorExample Error-Response1:
        # HTTP/1.1 Existing Operation Id
        #   {
        #     "error": "has already been taken"
        #   }
        # @apiError MissingToken invalid token .
        # @apiErrorExample Error-Response2:
        # HTTP/1.1 422 Missing token
        #   {
        #     "error": "Missing token"
        #   }
        # =end

        @money_op = MoneyOp.new(money_op_params)

        respond_to do |format|
          if @money_op.save
            format.json { render json: @money_op, status: :created, location: @money_op }
          else
            format.json { render json: @money_op.errors, status: :unprocessable_entity }
          end
        end

      end

      def operations_status

        # =begin
        # @api {get} /api/v1/operations_status?id={:id} 4-Update a Specific Operation Status
        # @apiVersion 0.3.0
        # @apiName UpdateOperationStatus
        # @apiGroup Money Operations
        # @apiExample Example usage:
        # curl -i http://localhost:3000/api/v1/operations_status?id=5
        # @apiSuccess {Number} id Money Operation unique ID.
        # @apiSuccess {String} opid Operation unique number.
        # @apiSuccess {Number} optype Operation type (1 for Sent & 2 for Receive).
        # @apiSuccess {Number} amount Operation amount.
        # @apiSuccess {Number} payment_gateway  Operation payment gateway (paypal,bitcoin,...).
        # @apiSuccess {Number} status Operation status (0 for pending & 1 for compeleted).
        # @apiSuccess {Date} payment_date  Payment operation creation date.
        # @apiSuccess {Number} payment_id Payment ID for the operation itself.
        # @apiSuccess {Number} user_id Operation user.
        # @apiSuccess {Date} created_at  Date created.
        # @apiSuccess {Date} updated_at  Date Updated.
        # @apiSuccessExample Success-Response:
        # HTTP/1.1 200 OK
        # {
        #   "status": 1,
        #   "id": 8,
        #   "opid": "PYL42W60CTGM",
        #   "optype": 1,
        #   "amount": "150",
        #   "payment_gateway": 1,
        #   "payment_date": "2018-08-01T13:31:00.000Z",
        #   "payment_id": 3,
        #   "user_id": 1,
        #   "created_at": "2018-08-01T11:33:49.055Z",
        #   "updated_at": "2018-08-01T15:45:10.000Z"
        # }
        # @apiError CardNotFound The ID of this Mony Operation was not found. 
        # @apiErrorExample Error-Response:
        # HTTP/1.1 404 Not Found
        #   {
        #     "error": "Card Not Found"
        #   }
        # =end

        @operations_status = MoneyOp.where(:id => params[:id].to_i).first
        flash[:error]= "Card Not Found"

        respond_to do |format|
            if @operations_status.status ==  0
              @operations_status.update(:status => 1)
              format.json { render json: @operations_status }
            elsif @operations_status.status ==  1
              @operations_status.update(:status => 0)
              format.json { render json: @operations_status }
            else
              format.json { render json: flash }
            end
        end
    
      end


      private
        # Use callbacks to share common setup or constraints between actions.
        def set_money_op
          @money_op = MoneyOp.find(params[:id])
        end

        # Never trust parameters from the scary internet, only allow the white list through.
        def money_op_params
          params.permit(:opid, :optype, :amount, :payment_gateway, :status, :payment_date, :payment_id, :user_id)
        end
    end
  end
end