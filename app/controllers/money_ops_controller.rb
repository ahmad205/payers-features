class MoneyOpsController < ApplicationController
  before_action :set_money_op, only: [:show]

  # GET list of Money Operations and display it
  # @return [id] Money Operation unique ID (Created automatically).
  # @return [opid] Operation unique number  (Created automatically and must be 12 digits and letters ).
  # @return [optype] Operation type (1 for Sent & 2 for Receive).
  # @return [amount] Operation amount.
  # @return [payment_gateway] Operation payment gateway (paypal,bitcoin,...).
  # @return [status] Operation status (0 for pending & 1 for compeleted).
  # @return [payment_date] Payment operation creation date.
  # @return [payment_id] Payment ID for the operation itself.
  # @return [user_id] Operation user id.
  # @return [created_at] Date created.
  # @return [updated_at] Date Updated.
  def index
    @money_ops = MoneyOp.all
  end

  # GET a spacific Money Operation and display it
  # @param [Integer] id Money operation unique ID.
  # @return [opid] Operation unique number (Created automatically and must be 12 digits and letters ).
  # @return [optype] Operation type (1 for Sent & 2 for Receive).
  # @return [amount] Operation amount.
  # @return [payment_gateway] Operation payment gateway (paypal,bitcoin,...).
  # @return [status] Operation status (0 for pending & 1 for compeleted).
  # @return [payment_date] Payment operation creation date.
  # @return [payment_id] Payment ID for the operation itself.
  # @return [user_id] Operation user id.
  # @return [created_at] Date created.
  # @return [updated_at] Date Updated.
  def show
  end

  # GET a new Money Operation
  # @param [Integer] id Money operation unique ID (Created automatically).
  # @param [String] opid Operation unique number (Created automatically and must be 12 digits and letters ).
  # @param [Integer] optype Operation type (1 for Sent & 2 for Receive).
  # @param [Integer] amount Operation amount.
  # @param [Integer] payment_gateway Operation payment gateway (paypal,bitcoin,...).
  # @param [Integer] status Operation status (0 for pending & 1 for compeleted).
  # @param [Date] payment_date Payment operation creation date.
  # @param [Integer] payment_id Payment ID for the operation itself.
  # @param [Integer] user_id Operation user id.
  def new
    @money_op = MoneyOp.new
  end

  # POST a new Money Operation and save it
  # @return [id] Money Operation unique ID (Created automatically).
  # @return [opid] Operation unique number (Created automatically and must be 12 digits and letters ).
  # @return [optype] Operation type (1 for Sent & 2 for Receive).
  # @return [amount] Operation amount.
  # @return [payment_gateway] Operation payment gateway (paypal,bitcoin,...).
  # @return [status] Operation status (0 for pending & 1 for compeleted).
  # @return [payment_date] Payment operation creation date.
  # @return [payment_id] Payment ID for the operation itself.
  # @return [user_id] Operation user id.
  # @return [created_at] Date created.
  # @return [updated_at] Date Updated.
  def create

    @money_op = MoneyOp.new(money_op_params)

    respond_to do |format|
      if @money_op.save
        format.html { redirect_to @money_op, notice: 'Money op was successfully created.' }
        format.json { render :show, status: :created, location: @money_op }
      else
        format.html { render :new }
        format.json { render json: @money_op.errors, status: :unprocessable_entity }
      end
    end
  end


  # Change Money Operations status
  # @param [Integer] id Money Operation unique ID (Created automatically).
  # @param [Integer] status Money Operation status (0 for Pending, 1 for Compeleted).
  # @return [id] Money Operation unique ID (Created automatically).
  # @return [opid] Operation unique number (Created automatically and must be 12 digits and letters ).
  # @return [optype] Operation type (1 for Sent & 2 for Receive).
  # @return [amount] Operation amount.
  # @return [payment_gateway] Operation payment gateway (paypal,bitcoin,...).
  # @return [status] Operation status (0 for pending & 1 for compeleted).
  # @return [payment_date] Payment operation creation date.
  # @return [payment_id] Payment ID for the operation itself.
  # @return [user_id] Operation user id.
  # @return [created_at] Date created.
  # @return [updated_at] Date Updated.
  def operations_status

    @operations_status = MoneyOp.where(:id => params[:id].to_i).first
    if @operations_status.status ==  0
      @operations_status.update(:status => 1)
      redirect_to(money_ops_path,:notice => 'Operation status was successfully updated to be Compeleted')
    elsif @operations_status.status ==  1
      @operations_status.update(:status => 0)
      redirect_to(money_ops_path,:notice => 'Operation status was successfully updated to be Pending')
    else
      redirect_to(money_ops_path,:notice => 'Sorry, something went wrong')
    end

  end


  private
    # Use callbacks to share common setup or constraints between actions.
    def set_money_op
      @money_op = MoneyOp.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def money_op_params
      params.require(:money_op).permit(:opid, :optype, :amount, :payment_gateway, :status, :payment_date, :payment_id, :user_id)
    end
end
