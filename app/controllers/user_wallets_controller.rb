class UserWalletsController < ApplicationController
  before_action :set_user_wallet, only: [:show]

  # GET list of Users Wallets and display it
  # @return [id] Users Wallets unique ID (Created automatically).
  # @return [currency] Users Wallets Currency (default = USD).
  # @return [amount] User Wallet amount.
  # @return [user_id] Wallet User ID.
  # @return [status] User Wallet status (0 for Disabled & 1 for Enabled).
  # @return [uuid] User Wallet Secure random number (Created automatically and must be 12 digits and letters ).
  # @return [created_at] Date created.
  # @return [updated_at] Date Updated.
  def index
    @user_wallets = UserWallet.all
  end

  # GET a spacific User Wallet and display it
  # @param [Integer] id User Wallet unique ID.
  # @return [currency] Users Wallets Currency (default = USD).
  # @return [amount] User Wallet amount.
  # @return [user_id] Wallet User ID.
  # @return [status] User Wallet status (0 for Disabled & 1 for Enabled).
  # @return [uuid] User Wallet Secure random number (Created automatically and must be 12 digits and letters).
  # @return [created_at] Date created.
  # @return [updated_at] Date Updated.
  def show
  end

  # GET a new User Wallet
  # @param [Integer] id User Wallet unique ID (Created automatically).
  # @param [String] currency Users Wallets Currency (default = USD).
  # @param [Float] amount User Wallet amount.
  # @param [Integer] user_id Wallet User ID.
  # @param [Integer] status User Wallet status (0 for Disabled & 1 for Enabled).
  # @param [String] uuid User Wallet Secure random number (Created automatically and must be 12 digits and letters).
  def new
    @user_wallet = UserWallet.new
  end

  # POST a new User Wallet and save it
  # @return [id] User Wallet unique ID.
  # @return [currency] Users Wallets Currency (default = USD).
  # @return [amount] User Wallet amount.
  # @return [user_id] Wallet User ID.
  # @return [status] User Wallet status (0 for Disabled & 1 for Enabled).
  # @return [uuid] User Wallet Secure random number (Created automatically and must be 12 digits and letters).
  # @return [created_at] Date created.
  # @return [updated_at] Date Updated.
  def create
    @user_wallet = UserWallet.new(user_wallet_params)

    respond_to do |format|
      if @user_wallet.save
        format.html { redirect_to @user_wallet, notice: 'User wallet was successfully created.' }
        format.json { render :show, status: :created, location: @user_wallet }
      else
        format.html { render :new }
        format.json { render json: @user_wallet.errors, status: :unprocessable_entity }
      end
    end
  end

  # Change User Wallet status
  # @param [Integer] id User Wallet unique ID (Created automatically).
  # @param [Integer] status User Wallet status (0 for Disabled & 1 for Enabled).
  # @return [id] User Wallet unique ID.
  # @return [currency] Users Wallets Currency (default = USD).
  # @return [amount] User Wallet amount.
  # @return [user_id] Wallet User ID.
  # @return [status] User Wallet status (0 for Disabled & 1 for Enabled).
  # @return [uuid] User Wallet Secure random number (Created automatically and must be 12 digits and letters).
  # @return [created_at] Date created.
  # @return [updated_at] Date Updated.
  def wallets_status

    @wallets_status = UserWallet.where(:id => params[:id].to_i).first
    if @wallets_status.status ==  0
      @wallets_status.update(:status => 1)
      redirect_to(user_wallets_path,:notice => 'Wallet was successfully Enabled')
    elsif @wallets_status.status ==  1
      @wallets_status.update(:status => 0)
      redirect_to(user_wallets_path,:notice => 'Wallet was successfully Disabled')
    else
      redirect_to(user_wallets_path,:notice => 'Sorry, something went wrong')
    end

  end

  # Transfer Balances between users wallets
  # @param [Integer] userfrom The sender user ID.
  # @param [Integer] userto The receiver user ID.
  # @param [Float] transferamount The transferred amount between two users.
  def transfer_balance   
  end

  # Post a transferred balance between two users
  # @return [userfrom] The sender user ID.
  # @return [userto] The receiver user ID.
  # @return [transferamount] The transferred amount between two users.
  # @return [new_balance_from] The sender new balance after transfer.
  # @return [new_balance_to] The receiver new balance after transfer.
  def transfer_balancepost
    @user_from = params[:userfrom]
    @user_to = params[:userto]
    @transfer_amount = params[:transferamount]
    @message = UserWallet.checktransfer(@user_from.to_i,@user_to.to_i,@transfer_amount)
    if @message == ""
      ActiveRecord::Base.transaction do
      @user_wallet_from = UserWallet.where(user_id: @user_from.to_i).first
      @user_wallet_to = UserWallet.where(user_id: @user_to.to_i).first
      @new_balance_from = @user_wallet_from.amount.to_f - @transfer_amount.to_f
      @new_balance_to = @user_wallet_to.amount.to_f + @transfer_amount.to_f
      @user_wallet_from.update(:amount => @new_balance_from )
      @user_wallet_to.update(:amount => @new_balance_to )
      redirect_to(user_wallets_path,:notice => "Balance was successfully transferred")
      end
    else
      redirect_to(user_wallets_path,:notice => @message )
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user_wallet
      @user_wallet = UserWallet.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def user_wallet_params
      params.require(:user_wallet).permit(:currency, :amount, :user_id, :status, :uuid)
    end
end
