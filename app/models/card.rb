class Card < ApplicationRecord

    validates_uniqueness_of :number
    validate :check_value
    validate :check_expiration
    before_save :default_values

    def default_values
        self.number ||= rand(1111111111111111...9999999999999999)
    end

    def check_value
      errors.add(:value, "must be equal or greater than 5 USD") if value < 5
    end

    def check_expiration
        errors.add(:expired_at, "Must be at least two days from now") if expired_at < Date.today + 2.days
    end

end
