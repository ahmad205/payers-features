class ChangeAmountToBeFloatInUserWallets < ActiveRecord::Migration[5.2]
  def change
    change_column :user_wallets, :amount, :float
  end
end
