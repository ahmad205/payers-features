class CreateCards < ActiveRecord::Migration[5.2]
  def change
    create_table :cards do |t|
      t.integer :number
      t.integer :value
      t.integer :status
      t.date :expired_at
      t.integer :user_id
      t.integer :invoice_id, :default => 0

      t.timestamps
    end
  end
end
