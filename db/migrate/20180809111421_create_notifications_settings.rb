class CreateNotificationsSettings < ActiveRecord::Migration[5.2]
  def change
    create_table :notifications_settings do |t|
      t.integer :user_id
      t.integer :money_transactions
      t.integer :pending_transactions
      t.integer :transactions_updates
      t.integer :help_tickets_updates
      t.integer :tickets_replies
      t.integer :account_login
      t.integer :change_password
      t.integer :verifications_setting

      t.timestamps
    end
  end
end
