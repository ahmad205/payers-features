# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
tansfer_balance_ratio = WalletsTransferRatio.create([{ key: 'max_transfer', value: 10, description: 'maximum wallets transfer balance' }, { key: 'min_transfer', value: 2, description: 'minimum wallets transfer balance' }])