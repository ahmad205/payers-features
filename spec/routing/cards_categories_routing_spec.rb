require "rails_helper"

RSpec.describe CardsCategoriesController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/cards_categories").to route_to("cards_categories#index")
    end

    it "routes to #new" do
      expect(:get => "/cards_categories/new").to route_to("cards_categories#new")
    end

    it "routes to #show" do
      expect(:get => "/cards_categories/1").to route_to("cards_categories#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/cards_categories/1/edit").to route_to("cards_categories#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/cards_categories").to route_to("cards_categories#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/cards_categories/1").to route_to("cards_categories#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/cards_categories/1").to route_to("cards_categories#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/cards_categories/1").to route_to("cards_categories#destroy", :id => "1")
    end

  end
end
