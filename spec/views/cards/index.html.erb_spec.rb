require 'rails_helper'

RSpec.describe "cards/index", type: :view do
  before(:each) do
    assign(:cards, [
      Card.create!(
        :number => 2,
        :value => 3,
        :status => 4,
        :expired_at => "",
        :user_id => 5,
        :invoice_id => 6
      ),
      Card.create!(
        :number => 2,
        :value => 3,
        :status => 4,
        :expired_at => "",
        :user_id => 5,
        :invoice_id => 6
      )
    ])
  end

  it "renders a list of cards" do
    render
    assert_select "tr>td", :text => 2.to_s, :count => 2
    assert_select "tr>td", :text => 3.to_s, :count => 2
    assert_select "tr>td", :text => 4.to_s, :count => 2
    assert_select "tr>td", :text => "".to_s, :count => 2
    assert_select "tr>td", :text => 5.to_s, :count => 2
    assert_select "tr>td", :text => 6.to_s, :count => 2
  end
end
