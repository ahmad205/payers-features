require 'rails_helper'

RSpec.describe "user_wallets/new", type: :view do
  before(:each) do
    assign(:user_wallet, UserWallet.new(
      :currency => "MyString",
      :amount => "MyString",
      :user_id => 1,
      :status => 1,
      :uuid => "MyString"
    ))
  end

  it "renders new user_wallet form" do
    render

    assert_select "form[action=?][method=?]", user_wallets_path, "post" do

      assert_select "input[name=?]", "user_wallet[currency]"

      assert_select "input[name=?]", "user_wallet[amount]"

      assert_select "input[name=?]", "user_wallet[user_id]"

      assert_select "input[name=?]", "user_wallet[status]"

      assert_select "input[name=?]", "user_wallet[uuid]"
    end
  end
end
