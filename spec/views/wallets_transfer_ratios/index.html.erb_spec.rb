require 'rails_helper'

RSpec.describe "wallets_transfer_ratios/index", type: :view do
  before(:each) do
    assign(:wallets_transfer_ratios, [
      WalletsTransferRatio.create!(
        :key => "Key",
        :value => 2.5,
        :description => "Description"
      ),
      WalletsTransferRatio.create!(
        :key => "Key",
        :value => 2.5,
        :description => "Description"
      )
    ])
  end

  it "renders a list of wallets_transfer_ratios" do
    render
    assert_select "tr>td", :text => "Key".to_s, :count => 2
    assert_select "tr>td", :text => 2.5.to_s, :count => 2
    assert_select "tr>td", :text => "Description".to_s, :count => 2
  end
end
